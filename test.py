print("This is purely a python test")
from os import system
system("echo This is a not pure python test")

import requests

# URL of the image
image_url = "https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/CERN_logo.svg/800px-CERN_logo.svg.png"

# Send a request to the image URL with streaming enabled
response = requests.get(image_url, stream=True)

# Check if the request was successful
if response.status_code == 200:
    chunk_size = 1024  # Define the chunk size in bytes
    chunk_count = 0    # Initialize chunk counter
    
    # Open a file to save the downloaded content
    with open("downloaded_image.jpg", "wb") as file:
        for chunk in response.iter_content(chunk_size=chunk_size):
            if chunk:  # Filter out keep-alive new chunks
                file.write(chunk)
                chunk_count += 1
    
    print(f"Image downloaded successfully in {chunk_count} chunks!")
else:
    print(f"Failed to download image. Status code: {response.status_code}")